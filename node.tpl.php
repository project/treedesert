<div class="node<?php print ($sticky) ? " sticky" : ""; ?>">
  <?php if ($page == 0) { ?>
    <h2><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
  <?php } else { ?>
    <?php print $picture ?>
    <em class="info"><?php print $submitted ?></em>
  <?php }; ?>
    <div class="content clearfix">
      <?php print $content ?>
    </div>
    <?php if ($links) { ?>
    <div class="info clearfix">
        <?php if ($terms) { print t("Posted in") . ' ' . $terms; }; ?>
        <?php if ($submitted) { ?>
          <?php print $submitted ?>
	      <br />
        <?php }; ?>
      <?php print $links ?></div>
    <?php }; ?>
	
</div>